#include <iostream>

using namespace std;

struct Node {
	int count;
	Node * next;
};



void insert(Node*,int);
bool Remove(Node*&,int);
void print(Node*);

int main()
{
	Node *node = new Node{1};
	Node *current = node;
	for (int i = 2; i < 6; i++){
	Node* ptr = new Node{i};
	current->next = ptr;
	current = ptr;
	}
	insert(node,6);
	Remove(node,1);
	print(node);
	
		return 0;
}

void insert(Node* first,int value){

	while(first->next != nullptr){
		first = first->next;
		}
	Node* second = new Node{value,nullptr};
	first->next = second;
}

bool Remove(Node *&first,int value){
	bool del = false;
	bool del1 = false;
	Node *curr = first;
	Node *ptr = curr;
	
	if (curr->count == value)
        del1 = del = true;
	
	curr = curr->next;
	
	while(curr != nullptr){
		if(curr->count == value){
			del = true;
			ptr->next = curr->next;
			delete curr;
			curr = ptr->next;
		}
		else{
			ptr = ptr->next;
			curr = curr->next;
		}
	}
	
	 if (del1)
    {
        curr = first->next;
        delete first;
        first = curr;
    }
	
	return del;
}

void print(Node *first){
	while(first != nullptr){
		cout << first->count << endl;
		first = first->next;
	}
}